# Uptodate
Program checking if your requirements are up to date.  
Only "Package==1.0" format is supported.

## Installations
```
pip install uptodate

```

## Examples
```
uptodate
uptodate requrements.txt
uptodate requrements.txt requirements-dev.txt
```
